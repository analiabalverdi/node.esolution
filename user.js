const router = require("express").Router();
//const { user } = require("./index.js");

router.get("/", async (req, res) => {
  try {
    const user = await req.context.models.users.find();
    return res.status(200).json({
      code: "202",
      message: "Accepted",
      data: user,
    });
  } catch (error) {
    return res.status(500).json({
      code: "500 - Internal Server Error",
      message: "Error, no se pudieron mostrar los usuarios",
      data: null,
    });
  }
});

router.get("/:userId", async (req, res) => {
  try {
    const user = await req.context.models.users.findById(req.params.userId);
    return res.status(200).json({
      code: "202",
      message: "Accepted",
      data: user,
    });
  } catch (error) {
    return res.status(410).json({
      code: "410 - Gone",
      message: "Error, usuario no existe",
      data: null,
    });
  }
});

router.post("/", async (req, res) => {
  try {
    const create = await req.context.models.users.create(req.body);
    return res.status(201).json({
      code: "201",
      message: "Ok. Usuario creado",
      data: create,
    });
  } catch (error) {
    return res.status(403).json({
      code: "403 - Forbidden",
      message: "No tiene autorización para la petición requerida",
      data: null,
    });
  }
});

router.put("/:userId", async (req, res) => {
  try {
    const update = await req.context.models.users.findOneAndUpdate(
      { userId: req.params.userId },
      { ...req.body },
      { new: true }
    );
    if (!update) {
        return res.status(404).json({
          code: "404",
          message: "NOT - FOUND",
          data: null,
        })}

    return res.status(200).json({
      code: "200",
      message: "Ok. Usuario actualizado",
      data: update,
    });
  } catch (error) {
    return res.status(422).json({
      code: "422 - Unprocessable Entity",
      message: "Error, volver a intentar ",
      data: null,
    });
  }
});

router.delete("/:userId", async (req, res) => {
  try {
    const eliminar = await req.context.models.users.findById(req.params.userId);
    if (eliminar) {
      await eliminar.remove();
    }
    return res.status(202).json({
      code: "202",
      message: "Ok. Usuario eliminado",
      data: eliminar,
    });
  } catch (error) {
    return res.status(401).json({
      code: "401 - Unauthorized",
      message: "Error al autenticar",
      data: null,
    });
  }
});

module.exports = router;

